﻿/// <binding Clean='clean' />

var gulp = require("gulp"),
  rimraf = require("rimraf"),
  fs = require("fs");

eval("var project = " + fs.readFileSync("./project.json"));

var paths = {
  bower: "./bower_components/",
  lib: "./" + project.webroot + "/lib/"
};

gulp.task("clean", function (cb) {
  rimraf(paths.lib, cb);
});

gulp.task("copy", ["clean"], function () {
  var bower = {
      "bootstrap": "bootstrap/dist/**/*.{js,map,css,ttf,svg,woff,eot}",
      "bootstrap-touch-carousel": "bootstrap-touch-carousel/dist/**/*.{js,css}",
      "hammer.js": "hammer.js/hammer*.{js,map}",
      "jquery": "jquery/dist/**/*.{js,map}",
      "angularjs": "angularjs/*.{js,map}",
      "angular-filter": "angular-filter/**/*.{js,map}",
      "angular-ui-grid": "angular-ui-grid/*.{js,css,eot,svg,ttf,woff}",
      "font-awesome": "font-awesome/**/*.{map,css,ttf,svg,woff,woff2,eot,otf}",
      "jquery-validation": "jquery-validation/jquery.validate.js",
      "jquery-validation-unobtrusive": "jquery-validation-unobtrusive/jquery.validate.unobtrusive.js"
  }

  for (var destinationDir in bower) {
    gulp.src(paths.bower + bower[destinationDir])
      .pipe(gulp.dest(paths.lib + destinationDir));
  }
});
