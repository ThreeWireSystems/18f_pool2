﻿var Map = {
    defaults: {
        center: new google.maps.LatLng(38.4987789, -98.035841),
        zoom: 4,
        mapTypeId: google.maps.MapTypeId.HYBRID
    },
    el: null,
    instance: null,
    isPrinting: false,
    data: [],
    locations: [],
    index: 0,
    markers: [],
    openWindow: {},
    heatMap: {},
    state: "",
    delay: 100,
    map_bounds: [],
    updateProgress: function(){},
    initialize: function (el) {
        var me = this;
        this.el = el;
        this.instance = new google.maps.Map(el, this.defaults);
        this.geocoder = new google.maps.Geocoder();
        this.map_bounds = new google.maps.LatLngBounds();
    },
    reset: function(){
        var me = this;
        me.data = [];
        me.index = 0;
        me.locations = [];
        me.map_bounds = new google.maps.LatLngBounds();
        if (me.heatMap.setMap != undefined) {
            me.heatMap.setMap(null);
        }
    },
    displayAddress: function(){
        var me = this;
        if (me.index < me.data.results.length) {
            setTimeout(function () {
                me.codeAddress(me.data.results[me.index].term + ', ' + me.state);
            }, me.delay);
        } else {
            me.heatMap = new google.maps.visualization.HeatmapLayer({
                data: me.locations,
                radius: 20
            });
            me.heatMap.setMap(me.instance);
            me.instance.fitBounds(me.map_bounds);
        }
    },
    codeAddress: function (address) {
        var me = this;
        this.geocoder.geocode({
            'address': address
        }, function (response, status) {
            switch (status) {
                case google.maps.GeocoderStatus.OK:
                    var loc = response[0].geometry.location;
                    me.locations.push({location: loc, weight: Math.max(me.data.results[me.index].count * 2.25, 5) });
                    me.index++;
                    me.map_bounds.extend(loc);
                    me.updateProgress( (me.index / me.data.results.length) * 100);
                    break;
                case google.maps.GeocoderStatus.OVER_QUERY_LIMIT:
                    me.delay++;
                    break;
                default:
                    alert("Geocode was not successful for the following reason: " + status);
                    break;
            }
            me.displayAddress();
        });
    },
    start: function (data, state) {
        var me = this;
        me.reset();
        me.data = data;
        me.state = state;
        me.displayAddress();
    },
    clear: function () {
        this.clearMarkers();
    },

    clearMarkers: function () {
        for (var i = 0, len = this.markers.length; i < len; i++) {
            var marker = this.markers[i];
            marker.setMap(null);
        }

        return this;
    }
};


var app = angular.module("app", []);

app.controller("mapCtrl", function ($scope, $http) {
    $scope.progress = 0;

    $scope.setProgress = function (val) {
        $scope.progress = Math.floor(val);
        $scope.$apply();
    }

    $scope.stateCount = function () {
        $http.get("/api/FdaFood/StateCount")
            .success(function (d) {
                $scope.stateData = d;
            });
    }

    $scope.getCityData = function (state) {
        $scope.state = state;
        $http.get("/api/FdaFood/CityCount?state=" + $scope.state.toLowerCase())
            .success(function (d) {
                $scope.progress = 0;
                Map.updateProgress = $scope.setProgress;
                Map.start(d, $scope.state);
            });
    }

    Map.initialize($("#map")[0]);
    $scope.stateCount();
});
angular.bootstrap(document, ['app']);