﻿using Microsoft.AspNet.Mvc;

namespace FaNgMvcBs2.Controllers
{
	public class HomeController : Controller
    {
        public IActionResult Index()
        {
			return View("EnforcementMap");
        }

		public IActionResult EnforcementMap()
		{
			return View();
		}

		public IActionResult LocationHeatmap()
		{
			return View();
		}

		public IActionResult StateMap()
		{
			return View();
		}

		public IActionResult UnitTest()
		{
			return View();
		}

	}
}
