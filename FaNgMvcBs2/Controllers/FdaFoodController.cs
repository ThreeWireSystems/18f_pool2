﻿using Microsoft.AspNet.Mvc;
//using FaNgMvcBs2.ViewModels;
// For more information on enabling Web API for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace FaNgMvcBs2.Controllers
{
    [Route("api/[controller]")]
    public class FdaFoodController : Controller
    {
		private readonly int limit = 25;

		private string ApiLocation()
		{
			var key = "40tqTRFnKf0bViGI2fkXF6kxZjQhqzY41UaeGa5D";
            return string.Format("https://api.fda.gov/food/enforcement.json?api_key={0}&", key);
        }

		private object ApiResponse(string query)
		{
			var httpClient = new System.Net.Http.HttpClient();
			var response = httpClient.GetAsync(ApiLocation() + query).Result;
			return response.Content.ReadAsStringAsync().Result;
		}

		// GET: api/values
		[HttpGet]
        public object Get()
        {
			return Get(1);
        }

		// GET api/values/5
		[HttpGet("{id}")]
		public object Get(int id)
		{
            return ApiResponse(string.Format("limit={0}&skip={1}", limit, limit * (id - 1)));
		}

		[HttpGet, ActionName("EnforcementCount"), Route("EnforcementCount/")]
		public object EnforcementCount()
		{
			return ApiResponse("count");
		}

		// GET api/values/5
		[HttpGet("id"), ActionName("CityCount"), Route("CityCount/")]
		public object CityCount(string state = "AL")
		{
			return ApiResponse(string.Format("search=state:{0}&count=city.exact", state));
		}

		// GET api/values/5
		[HttpGet, ActionName("StateCount"), Route("StateCount/")]
		public object GetStateCount()
		{
			return ApiResponse("count=state.exact");
		}

		//// POST api/values
		[HttpGet, ActionName("StateData"), Route("StateData")]
		public object GetStateData(int page, string state)
		{
			return ApiResponse(string.Format("search=state:{2}&limit={0}&skip={1}", limit, limit * (page - 1), state));
		}
	}
}
