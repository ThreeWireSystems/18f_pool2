﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using FdaService.Models.Food.Enforcement;


//namespace FaNgMvcBs2.ViewModels
//{
//    public class FoodEnforcementViewModel
//    {
//		public RootObject RootObject { get; set; }
//		public object Simplified()
//		{
//			if(RootObject == null)
//			{
//				throw new NullReferenceException("RootObject for Food is null.");
//			}
//			var resultSummary = RootObject.results.Select(r => new { product_description = r.product_description, product_quantity = r.product_quantity, reason_for_recall = r.reason_for_recall, status = r.status, state = r.state, country = r.country, city = r.city, recalling_firm = r.recalling_firm, classification = r.classification, recall_number = r.recall_number });
//			return new { meta = RootObject.meta, results = resultSummary };
//		}
//	}
//}
